!==========================================================================
!
! Routines:
!
! (1) input_co_q()      By ?         Last Modified 4/6/2015 (DYQ)
!
!     input: crys, gvec, syms, xct, kg, flagbz types
!
!     output: indexq_co, kgq, distgwfcoq types
!
!     Reads in the coarse grid valence wavefunctions from file WFNq_co
!     and distributes them between processors. The k-point grid is stored in kgq.
!
!  WARNING: Since this routine is not used by a working part of the code,
!  it has not been tested since implementation of new wfn format. --DAS
!  eqp corrections, Fermi level check, scissor shift probably needed.
!  UPDATED WARNING: This routine is now used by a working part of the code
!  and is compatible with the new WFN format. Fermi level check and scissor
!  shift still needed. --DYQ
!==========================================================================

#include "f_defs.h"

module input_co_q_m

  use checkbz_m
  use eqpcor_m
  use fullbz_m
  use global_m
  use input_utils_m
  use io_utils_m
  use misc_m
  use scissors_m
  use wfn_rho_vxc_io_m
  implicit none

  private

  public :: &
    input_co_q

contains

subroutine input_co_q(kp,kp_co,crys,gvec,kg,kgq,syms,xct,flagbz,distgwfcoq,eqp)
  type (kpoints), intent(inout) :: kp_co
  type (kpoints), intent(in) :: kp
  type (crystal), intent(in) :: crys
  type (gspace), intent(in) :: gvec
  type (symmetry), intent(in) :: syms
  type (xctinfo), intent(inout) :: xct
  type (grid), intent(in) :: kg
  integer, intent(in) :: flagbz
  type (tdistgwf), intent(out) :: distgwfcoq
  type (grid), intent(inout) :: kgq
  type (eqpinfo), intent(inout) :: eqp

  type (crystal) :: crysq
  type (gspace) :: gvecq
  type (symmetry) :: symsq
  type(kpoints) :: kpq
  type (wavefunction) :: wfnv
  character :: filenamev*20
  character :: fncor*32
  integer :: iunit_v
  integer :: irk,irks,ik,ikq,umk
  integer :: ii,jj,kk,is,isp,minband,maxband
  real(DP) :: qq_temp(3),delta,diffvol,vcell
  integer, allocatable :: indxkq(:)
  SCALAR, allocatable :: cg(:,:), cgarray(:)

  character(len=3) :: sheader
  integer :: iflavor
  type(gspace) :: gvec_kpt
  logical :: skip_checkbz, broken_degeneracy
  type(progress_info) :: prog_info

  PUSH_SUB(input_co_q)

!-------------------------------------------------------------------------
! Read the Wavefunction header and check that it`s compatible with WFN_co
!

  if (peinf%inode == 0) call open_file(26,file='WFNq_co',form='unformatted',status='old')
  sheader = 'WFN'
  iflavor = 0
  call read_binary_header_type(26, sheader, iflavor, kpq, gvecq, symsq, crysq, dont_warn_kgrid=xct%patched_sampling_co)
#ifdef DEBUG
  if (peinf%inode.eq.0) write(6,*) "gvec%ng: ", gvec%ng,gvecq%ng
#endif
  call check_trunc_kpts(xct%icutv, kpq)
  call check_header('WFN_fi', kp, gvec, syms, crys, 'WFNq_co', kpq, gvecq, symsq, crysq, is_wfn = .true.)

  SAFE_ALLOCATE(gvecq%components, (3, gvecq%ng))
  call read_binary_gvectors(26, gvecq%ng, gvecq%ng, gvecq%components)
  SAFE_DEALLOCATE_P(gvecq%components)

  if(any(kp_co%kgrid(1:3) /= kpq%kgrid(1:3))) then
    if(peinf%inode == 0) then
      write(0,*) 'WFN_co  kgrid = ', kp_co%kgrid(1:3)
      write(0,*) 'WFNq_co kgrid = ', kpq%kgrid(1:3)
    endif
    call die('kgrids for WFN_co and WFNq_co must be the same', only_root_writes = .true.)
  endif

  call get_volume(vcell,crysq%bdot)
  diffvol=abs(crysq%celvol-vcell)
  if (diffvol.gt.TOL_Small) then
    call die('volume mismatch', only_root_writes = .true.)
  endif
  
  call assess_degeneracies(kpq, kpq%el(kpq%mnband, :, :), kpq%mnband - 1, xct%efermi, TOL_Degeneracy)
  
  if(any(kpq%ifmax(:,:) == 0)) &
    call die("BSE codes cannot handle a system where some k-points have no occupied bands.", only_root_writes = .true.)
  
  kpq%nvband=minval(kpq%ifmax(:,:)-kpq%ifmin(:,:))+1
  kpq%ncband=kpq%mnband-maxval(kpq%ifmax(:,:))
  
  kgq%nr=kpq%nrk
  SAFE_ALLOCATE(kgq%r, (3,kgq%nr))
  kgq%r(1:3,1:kgq%nr)=kpq%rk(1:3,1:kpq%nrk)
  SAFE_ALLOCATE(indxkq, (kgq%nr))
  do ii=1,kgq%nr
    indxkq(ii) = ii
  enddo

  ! Check that finite Q is the same as the difference between kgq and kg
  
  ! DYQ TODO: Fix symmetries before commiting. For now, no symmetries for WFNq_co
  if (.true.) then
    call fullbz(crysq,symsq,kgq,1,skip_checkbz,wigner_seitz=.true.,paranoid=.true.)
  else
    call fullbz(crysq,symsq,kgq,symsq%ntran,skip_checkbz,wigner_seitz=.true.,paranoid=.true.)
  endif
  if (flagbz.eq.0.and.peinf%inode.eq.0) write(6,901)
  if (flagbz.eq.1.and.peinf%inode.eq.0) write(6,902)
901 format(1x,'Using symmetries to expand the shifted coarse-grid sampling')
902 format(1x,'No symmetries used in the shifted coarse-grid sampling')
    
!---------------------------------------------------------------------- 
! Find mapping between kgq and kg
!
  do ik=1,kgq%nf
    ikq = 0
    delta = 0.1d0
    do while ((delta .gt. TOL_Small) .and. (ikq.lt.kg%nf))
      ikq = ikq+1
      qq_temp(:) = kgq%f(:,ikq) - kg%f(:,ik) - xct%finiteq(:)
      do jj=1,3
        qq_temp(jj) = qq_temp(jj) - anint( qq_temp(jj) )
      enddo
      delta=sqrt((qq_temp(1))**2+(qq_temp(2))**2+(qq_temp(3))**2)
    enddo
    if (delta .gt. TOL_Small) then
      if(peinf%inode.eq.0) then
        write(0,*) '  Could not find point equivalent to ', (kg%f(ii,ik),ii=1,3)
      endif
      call die('Finite momentum not commensurate with kgrid of WFN_co',only_root_writes = .true.)
    endif
    xct%indexq(ik)=ikq
    ! kg%f and kgq%f may differ by a lattice vector near the zone edge
    do jj=1,3
      umk = nint(kg%f(jj,ik) - kgq%f(jj,ikq) + xct%finiteq(jj))
      kgq%f(jj,ikq) = kgq%f(jj,ikq) + dble(umk) !kgq(indexq(ik)) = kg(ikq)
      kgq%kg0(jj,ikq) = kgq%kg0(jj,ikq) + umk
    enddo
  enddo


!-----------------------------------------------------------------------
! If it exists, read eqp_co_q.dat for interpolation
  SAFE_ALLOCATE(eqp%evshift_co_q, (xct%nvb_co,kpq%nrk,kpq%nspin))
  eqp%evshift_co_q=0D0
  SAFE_ALLOCATE(eqp%ecshift_co_q, (xct%ncb_co,kpq%nrk,kpq%nspin))
  eqp%ecshift_co_q=0D0
  if (xct%eqp_co_q_corrections) then
    
    fncor = 'eqp_co_q.dat'

    SAFE_ALLOCATE(kpq%elda, (kpq%mnband, kpq%nrk, kpq%nspin))
    kpq%el(:,:,:) = kpq%el(:,:,:) - xct%avgpot / ryd
    kpq%elda(:,:,:) = kpq%el(:,:,:)
    
    minband = minval(kpq%ifmax(:,:)-xct%nvb_co+1)
    maxband = maxval(kpq%ifmax(:,:)+xct%ncb_co)
    call eqpcor(fncor,peinf%inode,peinf%npes,kpq, &
      minband,maxband,0,0,kpq%el,eqp%evshift_co_q,eqp%ecshift_co_q,1,0)

    !TODO: implement scissors schift

    ! now we call again to initialize the eqp arrays
    if(xct%eqp_co_corrections) then
      call eqpcor(fncor,peinf%inode,peinf%npes,kpq,0,0, &
        xct%nvb_co,xct%ncb_co,kp_co%el,eqp%evshift_co_q,eqp%ecshift_co_q,1,2,dont_write=.true.)
    endif

    if(any(kpq%ifmax(:,:) == 0)) & 
      call die("BSE codes cannot handle a system where some k-points have no occupied bands.", only_root_writes = .true.) 

    kpq%nvband=minval(kpq%ifmax(:,:)-kpq%ifmin(:,:))+1
    kpq%ncband=kpq%mnband-maxval(kpq%ifmax(:,:))
    
  endif !eqp_co_q_correction

!-----------------------------------------------------------------------
! Initialization of distributed wavefunctions

  distgwfcoq%nk=kgq%nr
  distgwfcoq%ngm=kpq%ngkmax
  distgwfcoq%ns=kpq%nspin
  distgwfcoq%nspinor=kpq%nspinor
  distgwfcoq%nv=xct%nvb_co
    
  ! FHJ: Use standard BLACS distribution for G-vectors
  distgwfcoq%block_sz = DIVUP(distgwfcoq%ngm, peinf%npes)
  ! ngl = local number of G-vectors that I own.
  distgwfcoq%ngl = NUMROC(distgwfcoq%ngm, distgwfcoq%block_sz, peinf%inode, 0, peinf%npes)
  ! Local to global index translation: ig_g = ig_l + tgl
  distgwfcoq%tgl = distgwfcoq%block_sz * peinf%inode

  SAFE_ALLOCATE(distgwfcoq%ng, (distgwfcoq%nk))
  SAFE_ALLOCATE(distgwfcoq%isort, (distgwfcoq%ngl,distgwfcoq%nk))
  SAFE_ALLOCATE(distgwfcoq%zv, (distgwfcoq%ngl,distgwfcoq%nv,distgwfcoq%ns*distgwfcoq%nspinor,distgwfcoq%nk))
  !SAFE_ALLOCATE(distgwfcoq%zc, (distgwfcoq%ngl,distgwfcoq%nc,distgwfcoq%ns*distgwfcoq%nspinor,distgwfcoq%nk))
      
  distgwfcoq%ng(:)=0
  distgwfcoq%isort(:,:)=0
  distgwfcoq%zv(:,:,:,:)=ZERO
  !distgwfcoq%zc(:,:,:,:)=ZERO
      
!-----------------------------------------------------------------------
! Read the wavefunctions and distribute

  SAFE_ALLOCATE(wfnv%isort, (gvec%ng))
  wfnv%nband=xct%nvb_co
  wfnv%nspin=kpq%nspin
  wfnv%nspinor=kpq%nspinor

  call progress_init(prog_info, 'reading wavefunctions (WFNq_co)', 'k-point', kpq%nrk)
  do irk=1,kpq%nrk
#ifdef DEBUG
    if (peinf%inode.eq.0) then
      write(6,*) "Reading kpoint: ",irk
    endif
#endif
    call progress_step(prog_info, irk)
    irks = 0
    do ii=1,kgq%nr
      if (indxkq(ii) == irk) then
        irks=ii
        exit
      endif
    enddo
    
    SAFE_ALLOCATE(gvec_kpt%components, (3, kpq%ngk(irk)))
    call read_binary_gvectors(26, kpq%ngk(irk), kpq%ngk(irk), gvec_kpt%components)
    
    SAFE_ALLOCATE(cg, (kpq%ngk(irk),kpq%nspin*kpq%nspinor))

    if(irks > 0) then
      do ii = 1, kpq%ngk(irk)
        call findvector(wfnv%isort(ii), gvec_kpt%components(:, ii), gvec)
        if (wfnv%isort(ii) == 0) call die('Could not find g-vector.')
      enddo

      wfnv%ng=kpq%ngk(irk)
      if(peinf%inode == 0) then
        SAFE_ALLOCATE(wfnv%cg, (wfnv%ng,wfnv%nband,wfnv%nspin*wfnv%nspinor))
        SAFE_ALLOCATE(cgarray, (kpq%ngk(irk)))
      endif
    endif

    SAFE_DEALLOCATE_P(gvec_kpt%components)

! Loop over the bands
    do ii=1,kpq%mnband

      call read_binary_data(26, kpq%ngk(irk), kpq%ngk(irk), kpq%nspin*kpq%nspinor, cg, bcast=.false.)
        
      if(irks == 0) cycle
      
      if(peinf%inode == 0) then ! set up wfnv on root only
        do is=1, kpq%nspin
          ! Only need valence wave functions
          if (ii .gt. (kpq%ifmax(irk,is)-xct%nvb_co) .and. ii .le. (kpq%ifmax(irk,is))) then
            
            do isp=1, kpq%nspinor
              do kk=1, kpq%ngk(irk)
                cgarray(kk)=cg(kk, is*isp)
              enddo
              if (peinf%verb_debug) then
                write(6,'(a,3(1x,i0),2(1x,f18.13))') 'input_co_q', irks, ii, is*isp, cgarray(1)
              endif
              wfnv%cg(1:wfnv%ng,kpq%ifmax(irk,is)-ii+1,is*isp)=cgarray
            enddo
            call checknorm('WFNq_co',ii,irks,kpq%ngk(irk),is,kpq%nspinor,cg(:,:))
          end if
        end do ! loop over spins
      endif ! peinf%inode=0  
    enddo ! ii (loop over bands) 
    SAFE_DEALLOCATE(cg)
    if(peinf%inode == 0) then
      SAFE_DEALLOCATE(cgarray)
    endif
      
#ifdef MPI
    ! broadcast valence wavefunction at irk to all other procs
    if (peinf%inode.ne.0) then
      SAFE_ALLOCATE(wfnv%cg, (wfnv%ng,wfnv%nband,wfnv%nspin*wfnv%nspinor))
    endif
    call MPI_BCAST(wfnv%cg(1,1,1),wfnv%ng*wfnv%nband*wfnv%nspin*wfnv%nspinor, &
      MPI_SCALAR,0,MPI_COMM_WORLD,mpierr)
#endif

    distgwfcoq%ng(irks)=wfnv%ng
    do ii=1,distgwfcoq%ngl
      if (ii+distgwfcoq%tgl.le.wfnv%ng) &
        distgwfcoq%isort(ii,irks)=wfnv%isort(ii+distgwfcoq%tgl)
    enddo
    ! copy wfnv%cg to distgwfcoq%zv for g-vectors owned by current proc
    do kk=1,distgwfcoq%ns*distgwfcoq%nspinor
      do jj=1,distgwfcoq%nv
        do ii=1,distgwfcoq%ngl
          if (ii+distgwfcoq%tgl.le.wfnv%ng) then 
            distgwfcoq%zv(ii,jj,kk,irks)=wfnv%cg(ii+distgwfcoq%tgl,jj,kk)
          endif
        enddo
      enddo
    enddo
    
    SAFE_DEALLOCATE_P(wfnv%cg)
  enddo ! irk (loop over k-points)  
  call progress_free(prog_info)
  
  SAFE_DEALLOCATE_P(wfnv%isort)
  SAFE_DEALLOCATE(indxkq)
  
  if (peinf%inode.eq.0) then
    write(6,'(/,1x,a)') 'Coarse-grid wavefunctions read from file WFNq_co:'
    write(6,'(1x,a,i0)') '- Number of k-points in irreducible BZ: ', kgq%nr
    write(6,'(1x,a,i0)') '- Number of k-points in full BZ: ', kgq%nf
    if (peinf%verb_high) then
      write(6,'(1x,a)') '- Listing all k-points:'
      write(6,'(1(2x,3(1x,f10.6)))') (kgq%r(:,jj), jj=1,kgq%nr)
    endif
    call close_file(26)
  endif ! node 0  
  SAFE_DEALLOCATE_P(kpq%rk)
  SAFE_DEALLOCATE_P(kpq%ifmin)
  SAFE_DEALLOCATE_P(kpq%ifmax)
  SAFE_DEALLOCATE_P(kpq%el)
  SAFE_DEALLOCATE_P(kp_co%rk)
  SAFE_DEALLOCATE_P(kp_co%ifmin)
  SAFE_DEALLOCATE_P(kp_co%ifmax)
  SAFE_DEALLOCATE_P(kp_co%el)  

  POP_SUB(input_co_q)
  
  return
end subroutine input_co_q

end module input_co_q_m

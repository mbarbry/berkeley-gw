!==============================================================================
!
! Utilities:
!
! (1) summarize_eigenvectors()       Originally By JRD,CHP   Last Modified: 3/16/2018 (GKA)
!
!     This program prints some useful information about the exciton wavefunction.
!
!==============================================================================

#include "f_defs.h"

program summarize_eigenvectors

  use global_m
  use evecs_m

  implicit none

  type(evecs_t) :: evecs

  integer :: ns, nc, nv, nk, ii, ic, iv, is, ik, ikmax
  integer :: ijk, nexc, ninfile
  integer :: up=10
  logical :: tda
  real(DP) :: energy, weight, this_weight, wmax, emax, emin
  real(DP), allocatable :: energies(:)
!
  character*20, allocatable :: filename(:)
  
  call open_file(unit=up,file='summarize_eigenvectors.inp',status='old')
  
  read(up,*) tda
  read(up,*) ninfile
  read(up,*) emin, emax
  read(up,*) nexc
  if (nexc .gt. 0) then
    SAFE_ALLOCATE(energies, (nexc))
    SAFE_ALLOCATE(filename, (nexc))
    do ijk =1, nexc
      read(up,*) energies(ijk)
    enddo
    do ijk =1, nexc
      write(filename(ijk),'(a8,i2.2)') 'exciton_',ijk
    enddo
  endif
  close (up) 

  evecs%tda = tda

  ! Open eigenvector file and read header
  call evecs%open_read_eigenvectors_bin()
  call evecs%read_header_bin()

  ! Print out information found in file
  write(6,'(a)')
  write(6,'(a)') 'Reading eigenvectors'
  write(6,'(a)')
  write(6,'(a,4i5)') ' ns, nv, nc, nk = ',evecs%ns,evecs%nv,evecs%nc,evecs%nk
  write(6,'(a,i8)') ' nmat = ',evecs%nmat
  write(6,'(a)')
  write(6,'(a)') 'kpoints follow:'
  do ik=1,evecs%nk
    write(6,'(i5,3f10.5)') ik, evecs%kk(:,ik)
  enddo
  write(6,'(a)')
  write(6,'(a)') 'exciton energies follow (eV)'
  write(6,'(a)') 'wtot = sum_k |A_vck|^2. wmax = max_k |A_vck|^2. |A_vc (ikmax)|^2 = wmax.'

  ns=evecs%ns; nk=evecs%nk; nv=evecs%nv; nc=evecs%nc
  if (ninfile .eq. 0) ninfile = evecs%neig
  
  ! We will be reading a single eigenvector at a time
  evecs%neig_me = 1
  evecs%meig = 1
  evecs%neig = 1

  ! Allocate memory
  call evecs%alloc(with_Avc=.true.)

  do ii=1,ninfile
    call evecs%read_single_eigenvectors_bin()

    energy = evecs%evals(1)
    if ((energy > emax) .or. (energy < emin)) cycle

    call evecs%reshape_Avc()

    write(6,'(a)')
    write(6,'(a)')
    write(6,'(a,i5,f10.5)') ' Special analysis for state ',ii,energy
    write(6,'(2a5,3a10)') 'c','v','wtot','wmax','ikmax'
    do ic=1,nc
      do iv=1,nv
        weight = 0.0d0
        wmax = 0.0d0
        do ik=1,nk
          do is=1,ns
            if ( tda ) then
              this_weight = abs(evecs%Avc_r(is,iv,ic,ik,1))**2
            else 
              this_weight = MYCONJG(evecs%Avc_l(is,iv,ic,ik,1))*evecs%Avc_r(is,iv,ic,ik,1) + &
                            MYCONJG(evecs%Bvc_l(is,iv,ic,ik,1))*evecs%Bvc_r(is,iv,ic,ik,1) 
            end if
            weight=weight+this_weight
            if (this_weight > wmax) then
              wmax = this_weight
              ikmax = ik
            endif
          enddo
        enddo
        write(6,'(2i5,2f10.5,i10)') ic, iv, weight, wmax, ikmax
      enddo
    enddo
    
    do ijk = 1, nexc
      if (abs(energy - energies(ijk)) .le. 1d-5) then
        write(6,'(a,i6,f12.6)') 'Calculating A(k) for :',ijk,energies(ijk)
        call open_file(unit=200+ijk,file=filename(ijk),status='replace')
        write(200+ijk, '(a,a9,2a10,a18)') '#', 'kx', 'ky', 'kz', 'sum |A(k)|^2'
        do ik=1,nk
          weight = 0.0d0
          do ic=1,nc
            do iv=1,nv
              do is=1,ns
                if ( tda ) then
                  this_weight = abs(evecs%Avc_r(is,iv,ic,ik,1))**2
                else 
                  this_weight = MYCONJG(evecs%Avc_l(is,iv,ic,ik,1))*evecs%Avc_r(is,iv,ic,ik,1) + &
                                MYCONJG(evecs%Bvc_l(is,iv,ic,ik,1))*evecs%Bvc_r(is,iv,ic,ik,1) 
                end if 
                weight = weight + this_weight 
              enddo
            enddo
          enddo
          write(200+ijk,'(3f10.5,f18.5)') evecs%kk(1:3,ik),weight
        enddo
        call close_file(200+ijk)
      endif
    enddo
      
  enddo
  write(6,'(a)')

  call evecs%close_eigenvectors_bin()
  call evecs%free()
  
end program summarize_eigenvectors

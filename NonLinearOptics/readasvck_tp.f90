!=============================================================================
!
! Routines:
!
! (1) readasvck_tp()    Orginally By JRD        Last Modified 6/6/2008 (JRD)
!
!     This routine reads in eigenvectors and calculates (by calling oscstrength)
!     the two-photon oscillator strength of a system.
!
!=============================================================================

#include "f_defs.h"

module readasvck_tp_m

  use global_m
  use oscstrength_m
  use timing_m, only: timing => extra_timing
  implicit none

  private
  public :: readAsvck_tp

contains

subroutine readasvck_tp(win,nmat,neig,s1,xct,nmatown, &
  osctotal,enarray,nk,k,nkpt,nmown,myown)
  type (windowinfo), intent(in) :: win
  integer, intent(in) :: nmat
  integer, intent(in) :: neig
  SCALAR, intent(in) :: s1(:) !< (nmat)
  type (xctinfo), intent(inout) :: xct
  integer, intent(out) :: nmatown
  real(DP), intent(out) :: osctotal(:) !< (neig), enarray(neig)
  real(DP), intent(out) :: enarray(:) !< (neig)
  integer, intent(out) :: nk
  integer, intent(in) :: nkpt
  real(DP), intent(out) :: k(:,:) !< (3,nkpt)
  integer, intent(out) :: nmown(:) !< (neig), myown(neig)
  integer, intent(out) :: myown(:) !< (neig)

  integer :: ns, nc, nv, i, m, unit, c, v, s, j
  integer :: ncounter, nblock, nmatold
  integer :: iflagfound
  integer :: ikcvs,iimax
  integer :: ik
  integer, allocatable :: myowni(:),imax(:,:),imax2(:,:)
  integer :: nloop, nwritefreq
  real(DP) :: e
  real(DP), allocatable :: kosc(:), osc0arrayt(:),dummy0t(:)
  real(DP), allocatable :: max1(:,:),max2(:,:)
  real(DP) :: nmax
  SCALAR, allocatable :: Aread(:),A(:,:,:,:,:),Af(:,:,:,:), &
    A1(:,:,:,:),osc0array(:),sumosc(:),sumosc2(:)
  SCALAR :: osc,tmp
  
  PUSH_SUB(readasvck_tp)
  
  unit = 10
  iflagfound=0

!-----------------------------------------
! Divide up states

  SAFE_ALLOCATE(myowni, (neig))
  
  nmatown = 0
  myown = 0
  do i = 1, neig
    nmown(i) = mod(i-1,peinf%npes)
    if (nmown(i) .eq. peinf%inode) then
      nmatown=nmatown+1
      myown(i)=nmatown
      myowni(nmatown)=i
    endif
  enddo
  
  call open_file(unit=unit,file='eigenvectors',form='unformatted',status='old')
  read(unit) ns
  read(unit) nv
  read(unit) nc
  read(unit) nk
  nmatold = ns*nv*nc*nk
  nblock = nv*nc*ns

! JRD: Debug

  if (peinf%inode .eq. 0) then
    write(6,*) ' '
    write(6,*) "------------------------------------------"
    write(6,*) "Reading eigenvectors"
    write(6,*) " "
    write(6,*) 'From   A: ns,nv,nc,nk = ',ns,nv,nc,nk
    write(6,*) 'From inp: ns,nv,nc,nk = ',xct%nspin,xct%nvb_fi,xct%ncb_fi,xct%nkpt_fi
    write(6,*) 'neig, nmatold:', neig, nmatold
    write(6,*) 'Proc, nmatown: ', peinf%inode,nmatown
    write(6,*) ' '
  end if
  
  SAFE_ALLOCATE(osc0arrayt, (nmatold))
#ifdef MPI
  SAFE_ALLOCATE(dummy0t, (nmatold))
#endif
  osc0arrayt=0d0
  SAFE_ALLOCATE(kosc, (nk))
  read(unit) k(:,:)
  SAFE_ALLOCATE(Aread, (nmatold))
  SAFE_ALLOCATE(A, (nmatown,ns,nv,nc,nk))
  SAFE_ALLOCATE(Af, (ns,nv,nc,nk))
  SAFE_ALLOCATE(A1, (ns,nv,nc,nk))
  SAFE_ALLOCATE(osc0array, (nmatown))
  
  osc0arrayt = 0D0
  
  do i=1,nmatold

!        if (peinf%inode .eq. 0) write(6,*) 'i =', i

    read(unit) e
    enarray(i) = e
    read(unit) Aread(:)
    if (nmown(i) .eq. peinf%inode)  then
      m=0
      do ik=1,nk
        do c=1,nc
          do v=1,nv
            do s=1,ns
              m = m+1
              A(myown(i),s,v,c,ik) = Aread(m)
            enddo
          enddo
        enddo
      enddo

!          if (peinf%inode .eq. 0) write(6,*) 'read A'

!-------------------------------
! Calculate osctrength from |0>

      tmp = 0D0
      do ik=1,nk
        do c=1,nc
          do v=1,nv
            do s=1,ns
              ikcvs= s + (v - 1 + (c + nv - 1 + (ik - 1)* &
                (nv+nc))*(nv+nc))*ns
              tmp = tmp+MYCONJG(A(myown(i),s,nv-v+1,c,ik))*s1(ikcvs)
            enddo
          enddo
        enddo
      enddo
      
!          if (peinf%inode .eq. 0) write(6,*) 'Finished Osc0'

! Check whether correct phase

      osc0array(myown(i))=tmp
      osc0arrayt(i)=abs(tmp)**2
    endif
    
  enddo
  
  call close_file(unit)

! JRD: Share osc0 for debugging purposes.

#ifdef MPI
  dummy0t=osc0arrayt
  call MPI_ALLREDUCE(dummy0t,osc0arrayt,nmatold,MPI_REAL_DP,MPI_SUM,MPI_COMM_WORLD,mpierr)
  SAFE_DEALLOCATE(dummy0t)
#endif
  
  if (peinf%inode .eq. 0) then
    write(6,*) ' '
    write(6,*) 'Finished Reading A'
    write(6,*) 'Starting Calculation of Two-Photon Osc. Strength'
    write(6,*) ' '
  endif

!-------------------------------
! Loop over Final States
  
  SAFE_ALLOCATE(sumosc, (neig))
  SAFE_ALLOCATE(sumosc2, (neig))
  SAFE_ALLOCATE(max1, (peinf%npes,neig))
  SAFE_ALLOCATE(imax, (peinf%npes,neig))
  SAFE_ALLOCATE(max2, (peinf%npes,neig))
  SAFE_ALLOCATE(imax2, (peinf%npes,neig))
  
  max1=0d0
  imax=0
  
  sumosc = 0D0
  max1 = 0D0
  imax = 0D0
  
  if ( win%nstates > 0) then
    nloop = min(win%nstates,neig)
  else
    nloop=neig
  endif
  
  nwritefreq = 1
  if (neig .ge. 100) nwritefreq = 10
  if (neig .ge. 1000) nwritefreq = 100
  
  do i = 1, nloop
    
    if (mod(i,neig/nwritefreq) .eq. 0) then
      if (peinf%inode .eq. 0) then
        write(6,*) 'Starting Final State', i, neig
      endif
    endif

! Broadcast Final State

    if (nmown(i) .eq. peinf%inode) then
      Af(:,:,:,:)=A(myown(i),:,:,:,:)
    endif

!        if (peinf%inode .eq. 0) write(6,*) 'BCAST Begin', i, neig
    call timing%start(timing%os_comm)

#ifdef MPI
    call MPI_BCAST(Af(1,1,1,1),neig,MPI_SCALAR,nmown(i), &
      MPI_COMM_WORLD,mpierr)
#endif

    call timing%stop(timing%os_comm)
    call timing%start(timing%os_sums)

!        if (peinf%inode .eq. 0) write(6,*) 'BCAST End', i, neig
!        if (peinf%inode .eq. 0) write(6,*) 'Loop Begin', i, neig

!-------------------------------
! Loop over states proc owns

    sumosc(i)=0d0

    do j = 1, nmatown

! We need the term j=i for normalization including single-photon spectra.

!          if (j .ne. myown(i)) then

      A1(:,:,:,:)=A(j,:,:,:,:)
      ncounter = j
      
      osc=0d0
      
      call oscstrength(nmat,nk,ns,xct,A1,Af,s1,osc)
      
      sumosc(i)=sumosc(i)+MYCONJG(osc)*osc0array(j) &
        / ((enarray(myowni(j))-(enarray(i)/2d0))/ryd)

! POSSIBLE FIX!!!
! JRD: enarray is the culprit?? should myowni(j)!!!!!!!

!          endif

      nmax=abs(osc*osc0array(j) &
        / ((enarray(myowni(j))-(enarray(i)/2d0))/ryd))**2
      
      if (nmax .gt. max1(peinf%inode+1,i)) then
        max1(peinf%inode+1,i)=nmax
        imax(peinf%inode+1,i)=myowni(j)
      endif
      
    enddo

    call timing%stop(timing%os_sums)
        !if (peinf%inode .eq. 0) write(6,*) 'Loop End', i, neig

  enddo

!-------------------------------
! Now collapse osctotal

#ifdef MPI
  sumosc2 = sumosc
  max2=max1
  imax2=imax
  call MPI_ALLREDUCE(sumosc2,sumosc,neig, &
    MPI_SCALAR,MPI_SUM,MPI_COMM_WORLD,mpierr)
  call MPI_ALLREDUCE(max2(1,1),max1(1,1),neig*peinf%npes, &
    MPI_REAL_DP,MPI_SUM,MPI_COMM_WORLD,mpierr)
  call MPI_ALLREDUCE(imax2(1,1),imax(1,1),neig*peinf%npes, &
    MPI_INTEGER,MPI_SUM,MPI_COMM_WORLD,mpierr)
#endif

! Find highest contributing middle state
! and square osctotal
  if (peinf%inode .eq. 0) then
    call open_file(11,file='osc.dat',form='formatted',status='replace')
  endif

  do i = 1, neig
    
    nmax = 0d0
    
    do j = 1,peinf%npes
      if (max1(j,i) .gt. nmax) then
        nmax = max1(j,i)
        iimax = imax(j,i)
      endif
    enddo
    
    osctotal(i)=abs(sumosc(i))**2
    
    if (peinf%inode .eq. 0) then
      write(11,'(i8,3e16.8,i8,e16.8)') i,enarray(i),osc0arrayt(i),osctotal(i),iimax,nmax
    endif
    
  enddo
  if (peinf%inode .eq. 0) then
    call close_file(11)
  endif
  
  SAFE_DEALLOCATE(sumosc2)
  SAFE_DEALLOCATE(sumosc)
  SAFE_DEALLOCATE(Aread)
  SAFE_DEALLOCATE(A)
  SAFE_DEALLOCATE(A1)
  SAFE_DEALLOCATE(Af)
  SAFE_DEALLOCATE(osc0array)
  SAFE_DEALLOCATE(osc0arrayt)
  SAFE_DEALLOCATE(max1)
  SAFE_DEALLOCATE(max2)
  SAFE_DEALLOCATE(imax)
  SAFE_DEALLOCATE(imax2)
  SAFE_DEALLOCATE(myowni)

  POP_SUB(readasvck_tp)

  return
end subroutine readasvck_tp

end module readasvck_tp_m

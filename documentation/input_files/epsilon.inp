# epsilon.inp

# Energy cutoff for the dielectric matrix, in Ry. The dielectric matrix
# $\varepsilon_{GG'}$ will contain all G-vectors with kinetic energy $|q+G|^2$
# up to this cutoff.
#[unit=Ry]
epsilon_cutoff           35.0

# Total number of bands (valence+conduction) to sum over. Defaults to the
# number of bands in the WFN file minus 1.
#number_bands            1000

# This flags specifies the frequency dependence of the inverse dielectric matrix:
#
# - Set to 0 to compute the static inverse dielectric matrix (default).
# - Set to 2 to compute the full frequency dependent inverse dielectric matrix.
# - Set to 3 to compute the two frequencies needed for Godby-Needs GPP model.
#frequency_dependence 0

# Plasma frequency (eV) needed for the contour-deformation method (i.e.,
# [[frequency_dependence]]==2). The exact value
# is unimportant, especially if you have enough imaginary frequency points. We
# recommend you keep this value fixed at 2 Ry.
#plasma_freq 27.21138506d0

# For [[frequency_dependence]]==3, the value of the purely imaginary frequency, in eV:
#imaginary_frequency 27.21138506d0


# ### Parameters for full-frequency-dependent calculations
# Three methods are available, depending on the value of [[frequency_dependence_method]]:
#
# - 0: Real-axis formalism with Adler-Wiser formula:
#
#     - A uniform frequency grid is set up from [[init_frequency]] (defaults to 0),
#       up to [[low_frequency_cutoff]], with a spacing of [[delta_frequency]] between
#       two frequencies.
#     - A non-uniform frequency grid is setup from [[low_frequency_cutoff]] to
#       [[high_frequency_cutoff]], where the frequency spacing gets increased by
#       [[delta_frequency_step]].
#
# - 1: Real-axis formalism with spectral method:
#
#     - A uniform frequency grid is set up from [[init_frequency]] (defaults to 0),
#       up to [[low_frequency_cutoff]], with a spacing of [[delta_frequency]] between
#       two frequencies.
#     - A non-uniform frequency grid is setup from [[low_frequency_cutoff]] to
#       [[high_frequency_cutoff]], where the frequency spacing gets increased by
#       [[delta_frequency_step]].
#     - A separate frequency grid is set-up for the spectral function. The variables
#       [[init_sfrequency]], [[delta_sfrequency]], [[delta_sfrequency_step]],
#       [[sfrequency_low_cutoff]], and [[sfrequency_high_cutoff]] define this grid, in
#       an analogy to the flags used to define the grid for the polarizability matrix.
#
# - 2: Contour-deformation formalism with Adler-Wiser formula (**default**).
#
#    - A uniform frequency grid is set up from [[init_frequency]] (defaults to 0),
#      up to [[low_frequency_cutoff]], with a spacing of [[delta_frequency]] between
#      two frequencies.
#    - A frequency grid with [[number_imaginary_freqs]] is set-up on the imag axis.
#
#
# Full frequency dependence method for the polarizability, if [[frequency_dependence]]==2:
#
# - 0: Real-axis formalism, Adler-Wiser formula.
# - 1: Real-axis formalism, spectral method (PRB 74, 035101, (2006))
# - 2: Contour-deformation formalism, Adler-Wiser formula.
#frequency_dependence_method 2
#
#
# Broadening parameter for each val->cond transition.
# It should correspond to the energy resolution due to k-point sampling,
# or a number as small as possible if you have a molecule.
# The default value is 0.1 for method 0 and 1, and 0.25 for method 2.
#broadening 0.1
#
# Lower bound for the linear frequency grid.
#init_frequency 0.0
#
# Upper bound for the linear frequency grid.
# For methods 0 and 1, it is also the lower bound for the non-uniform frequency grid.
# Should be larger than the maximum transition, i.e., the
# energy difference between the highest conduction band and the lowest valence band.
# The default is 200 eV for method 0 and 1, and 10 eV for method 2.
# For method 2, you can increase frequency_low_cutoff if you wish to use
# the Sigma code and look into QP states deep in occupied manifold or high in
# the unoccupied manifold.
#frequency_low_cutoff 200.0
#
# Frequency step for full-frequency integration for the linear grid
# Should be converged (the smaller, the better).
# For molecules, delta_frequency should be the same as broadening.
# Defaults to the value of broadening.
#delta_frequency 0.1
#
# Number of frequencies on the imaginary axis for method 2.
#number_imaginary_freqs 15
#
# Upper limit of the non-uniform frequency grid.
# Defaults to 4*[[frequency_low_cutoff]]
#frequency_high_cutoff 1000.0
#
# Increase in the frequency step for the non-uniform frequency grid.
#delta_frequency_step 1.0
#
# Frequency step for the linear grid for the spectral function method.
# Defaults to [[delta_frequency]].
#delta_sfrequency 0.1
#
# Increase in frequency step for the non-uniform grid for the spectral function method.
# Defaults to [[delta_frequency_step]]
#delta_sfrequency_step 1.0
#
# Upper bound for the linear grid for the spectral function method
# and lower bound for the non-uniform frequency grid.
# Defaults to [[frequency_low_cutoff]]
#sfrequency_low_cutoff 200.0
#
# Upper limit of the non-uniform frequency grid for the spectral function method.
# Defaults to [[frequency_low_cutoff]]
#sfrequency_high_cutoff 200.0


# Logging convergence of the head & tail of polarizability matrix with respect to conduction bands:
#
# - Set to -1 for no convergence test
# - Set to 0 for the 5 column format including the extrapolated values (default).
# - Set to 1 for the 2 column format, real part only.
# - Set to 2 for the 2 column format, real and imaginary parts.
#full_chi_conv_log -1

# qx qy qz 1/scale_factor is_q0
#
# scale_factor is for specifying values such as 1/3
# is_q0 = 0 for regular, non-zero q-vectors (read val WFNs from `WFN`)
# is_q0 = 1 for a small q-vector in semiconductors (read val WFNs from `WFNq`)
# is_q0 = 2 for a small q-vector in metals (read val WFNs from `WFN`)
# if present the small q-vector should be first in the list
# You can generate this list with `kgrid.x`: just set the shifts to zero and use
# same grid numbers as for `WFN`. Then replace the zero vector with q0.
#
begin qpoints
  0.000000    0.000000    0.005000   1.0   1
  0.000000    0.000000    0.062500   1.0   0
  0.000000    0.000000    0.125000   1.0   0
  0.000000    0.000000    0.187500   1.0   0
  0.000000    0.000000    0.250000   1.0   0
  0.000000    0.000000    0.312500   1.0   0
  0.000000    0.000000    0.375000   1.0   0
  0.000000    0.000000    0.437500   1.0   0
  0.000000    0.000000    0.500000   1.0   0
  0.000000    0.000000    0.562500   1.0   0
  0.000000    0.000000    0.625000   1.0   0
  0.000000    0.000000    0.687500   1.0   0
  0.000000    0.000000    0.750000   1.0   0
  0.000000    0.000000    0.812500   1.0   0
  0.000000    0.000000    0.875000   1.0   0
  0.000000    0.000000    0.937500   1.0   0
end

%include scissors.inp
%include common.inp

# Set this to use eigenvalues in eqp.dat and eqp_q.dat
# If not set, these files will be ignored.
#eqp_corrections

# Write the bare Coulomb potential $v(q+G)$ to file
#write_vcoul

# Matrix Element Communication Method (Chi Sum Comm). Default is [[gcomm_matrix]]
# which is good if $nk, nc, nv > nmtx, nfreq$. If $nk, nc, nv < nfreq, nmtx$
# ($nk, nv < nfreq$ since $nc \sim nmtx$), use gcomm_elements. Only [[gcomm_elements]]
# is supported with the spectral method.
#
#
#gcomm_matrix
#gcomm_elements


# Number of pools for distribution of valence bands
# The default is chosen to minimize memory in calculation
#number_valence_pools 1

# By default, the code computes the polarizability matrix, constructs
# the dielectric matrix, inverts it and writes the result to file epsmat.
# Use keyword skip_epsilon to compute the polarizability matrix and
# write it to file chimat. Use keyword skip_chi to read the polarizability
# matrix from file chimat, construct the dielectric matrix, invert it and
# write the result to file epsmat.
#
#
#skip_epsilon
#skip_chi

# (Full Frequency only) Calculates several frequencies in parallel. No "new"
# processors are used here, the chi summation is simply done in another order
# way to decrease communication. This also allows the inversion of multiple
# dielectric matrices simultaneously via ScaLAPACK, circumventing ScaLAPACK's
# scaling problems. Can be very efficient for system with lots of G-vectors and when
# you have many frequencies. In general gives speedup. However, in order to
# calculate N frequencies in parallel, the memory to store `pol%gme` is currently
# multiplied by N as well.
#
#nfreq_group 1

# EXPERIMENTAL FEATURES FOR TESTING PURPOSES ONLY

# 'unfolded BZ' is from the kpoints in the WFN file
# 'full BZ' is generated from the kgrid parameters in the WFN file
# See comments in Common/checkbz.f90 for more details
#
#
# Replace unfolded BZ with full BZ
#fullbz_replace
# Write unfolded BZ and full BZ to files
#fullbz_write

# The requested number of bands cannot break degenerate subspace
# Use the following keyword to suppress this check
# Note that you must still provide one more band in
# wavefunction file in order to assess degeneracy
#degeneracy_check_override

# Instead of using the RHO FFT box to perform convolutions, we automatically
# determine (and use) the smallest box that is compatible with your epsilon
# cutoff. This also reduces the amount of memory needed for the FFTs.
# Although this optimization is safe, you can disable it by uncommenting the
# following line:
#no_min_fftgrid

# Use this flag if you would like to restart your Epsilon calculation
# instead of starting it from scratch. Note that we can only reuse q-points
# that were fully calculated. This flag is ignored unless you are running
# the code with HDF5.
#restart

# Q-grid for the epsmat file. Defaults to the WFN k-grid.
#qgrid 0 0 0


# Use this option to generate an eps0mat file suitable for subsampled Sigma
# calculations. The only thing this flag does is to allow a calculation with
# more than one $q\rightarrow0$ points without complaining.
#subsample


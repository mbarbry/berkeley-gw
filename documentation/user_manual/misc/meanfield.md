# Mean-field calculations

The BerkeleyGW software package uses many-body perturbation-theory formalisms;
therefore, one needs to provide a reasonable mean-field starting point for the
perturbation-theory calculations. For most application, density-functional
theory (DFT) based on semilocal functionals provide a good starting point for
GW and GW-BSE calculations.

The following mean-field codes are currently supported by BerkeleyGW:

 - [Quantum ESPRESSO](http://www.quantum-espresso.org/){:target="_blank"}
 - [Abinit](https://www.abinit.org/){:target="_blank"}
 - [Octopus](http://octopus-code.org/){:target="_blank"}
 - [PARATEC](http://www.nersc.gov/users/software/applications/materials-science/paratec/){:target="_blank"}
 - [PARSEC](http://parsec.ices.utexas.edu/){:target="_blank"}
 - [EPM (Empirical Pseudopotential Method)](https://books.google.com/books?id=dmRTFLpSGNsC){:target="_blank"}
 - [Siesta](http://www.icmab.es/siesta/){:target="_blank"}

In addition, we include [a wrapper](bgw2sgw-overview.md) to convert mean-field
quantities to the [StochasticGW](http://stochasticgw.com) code.

We provide a library to easily write mean-field-related quantities in the
format used by BerkeleyGW.

BerkeleyGW requires the following quantities from mean-field codes:

 - Mean-field eigenvalues and eigenvectors, stored in `WFN` files, for
   arbitrary k-point grids.
 - The mean-field exchange-correlation matrix elements, `vxc.dat`, or
   exchange-correlation matrix in reciprocal space, `VXC`.
 - Ground-state charge density $\rho(G)$, stored in `RHO` -- only for
   calculations based on the generalized plasmon-pole (GPP) model.

For further information on how to use each mean-field code with the appropriate
wrapper for BerkeleyGW, we refer to the BerkeleyGW tutorials.


## Pitfalls

There are some notorious cases where typical DFT calculations might not provide
a good starting point for one-shot perturbation-theory calculations. Examples
include:

 - Germanium crystal, which is often predicted to be metallic at DFT. This
   issue can be remedied by either using another mean-field starting point, or
   performing some sort of self-consistent iteration, for instance, based on
   the static COHSEX approximation.

 - Molecules such as silane (SiH4), in which semilocal DFT often yields a LUMO
   orbital that bound, where in reality it is unbound. These systems can also
   be remedied by using a different starting mean-field point, or performing
   self-consistent GW calculations. For molecules, another common approach is
   known as best $G$, best $W$, where one picks one mean-field starting point
   to write the Green's function $G$ in $\Sigma=iGW$, such as Hartree-Fock, and
   another one to compute the polarizability $\chi^0$ used to construct $W$,
   such as LDA.

 - Some strongly correlated systems, especially those with partially filled $f$
   orbitals. Systems such as transition-metal oxides are often tackled with a
   Hubbard-type of correction scheme, such as DFT+U.

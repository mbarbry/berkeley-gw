!============================================================================
!
! Routines:
!
! (1) spinor_symmetries     Originally by BAB          Last Modified: 4/01/2014 (BAB)
!
!     Find the 2x2 matrix to rotate spinor components of wavefunctions,
!     given the integer rotation matrix in lattice coordinates.
!     This function must be called in all genwf files for spinor calculations.
!
!============================================================================

#include "f_defs.h"

module spinor_symmetries_m

  use global_m
  use random_m
  use misc_m
  use blas_m

  implicit none

  private


contains


end module spinor_symmetries_m

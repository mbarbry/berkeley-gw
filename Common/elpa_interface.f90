!===========================================================================
!
! Modules:
!
! elpa_interface_m    Originally By MDB      Last modified: 2017/10 (FHJ)
!
!   Functions, types, and interfaces for ELPA.
!   Note: you have to use ELPA 20170403 or later.
!
!============================================================================

#include "f_defs.h"

module elpa_interface_m
#ifdef USEELPA
  use global_m
  use scalapack_m
  use elpa

  implicit none

  private

  public :: diagonalize_elpa_complex

contains

  subroutine diagonalize_elpa_complex(N, Nev, scal, matrix, eigen_vect, eigenval)
    integer :: N, Nev
    type (scalapack), intent(in) :: scal
    complex(DP), intent(inout) :: matrix(scal%npr,scal%npc), eigen_vect(scal%npr,scal%npc)
    real(DP), intent(inout) :: eigenval(N)

    integer :: nblk
    integer :: np_rows, np_cols, na_rows, na_cols
    integer :: my_prow, my_pcol, mpi_comm_rows, mpi_comm_cols
    integer :: info, nprow, npcol
    complex(DP), parameter :: CZERO = (0d0,0d0)
    integer :: success
    class(elpa_t), pointer :: elpa_solver

    PUSH_SUB(diagonalize_elpa_complex)

    success = .true.
    eigen_vect = CZERO
    eigenval = 0d0
    nblk = scal%nbl
    np_rows = scal%nprow
    np_cols = scal%npcol
    na_rows = scal%npr
    na_cols = scal%npc
    my_prow = scal%myprow 
    my_pcol = scal%mypcol
    
    if (elpa_init(20170403) /= elpa_ok) then
      call die("ELPA API version not supported", only_root_writes=.true.)
    endif
    elpa_solver => elpa_allocate()
    ! set parameters decribing the matrix and it`s MPI distribution
    call elpa_solver%set("na", N, success)
    call elpa_solver%set("nev", nev, success)
    call elpa_solver%set("local_nrows", scal%npr, success)
    call elpa_solver%set("local_ncols", scal%npc, success)
    call elpa_solver%set("nblk", scal%nbl, success)
    call elpa_solver%set("mpi_comm_parent", MPI_COMM_WORLD, success)
    call elpa_solver%set("process_row", scal%myprow, success)
    call elpa_solver%set("process_col", scal%mypcol, success)
    success = elpa_solver%setup()
    ! if desired, set tunable run-time options
    call elpa_solver%set("solver", ELPA_SOLVER_2STAGE, success)

    ! use method solve to solve the eigenvalue problem to obtain eigenvalues
    ! and eigenvectors
    ! other possible methods are desribed in \ref elpa_api::elpa_t derived type
    call elpa_solver%eigenvectors(matrix, eigenval, eigen_vect, success)

    ! cleanup
    call elpa_deallocate(elpa_solver)
    call elpa_uninit()

  POP_SUB(diagonalize_elpa_complex)

  end subroutine
#endif

end module elpa_interface_m


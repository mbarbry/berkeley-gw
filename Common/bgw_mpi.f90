! File bgw_mpi.f90 automatically created from bgw_mpi.f90p by mako_preprocess.py.
! Do not edit the resulting file (bgw_mpi.f90) directly!

!>=========================================================================
!!
!! Module:
!!
!! bgw_mpi_m        Originally by FHJ     Last Modified 03/2018 (FHJ)
!!
!!     Wrappers around MPI functions. All functions work both with
!!     MPI and serial runs. Right now, only MPI_Bcast is supported.
!!     TODO: implement wrappers for MPI_Send, MPI_Recv, gather, etc.
!!
!!     This module uses python Mako template to generate Fortran code.
!!     Make sure to run the `mako_preprocess.py` if you edit the source
!!     .f90p file, and don`t edit the generated .f90 directly.
!!
!!=========================================================================

#include "f_defs.h"


module bgw_mpi_m
  use, intrinsic :: iso_c_binding
  use global_m

  implicit none

  interface bgw_bcast
    module procedure &
      bgw_bcast_int_0, &
      bgw_bcast_int_1, &
      bgw_bcast_int_2, &
      bgw_bcast_int_3, &
      bgw_bcast_int_4, &
      bgw_bcast_int_5, &
      bgw_bcast_int_6, &
      bgw_bcast_int_7, &
      bgw_bcast_real_0, &
      bgw_bcast_real_1, &
      bgw_bcast_real_2, &
      bgw_bcast_real_3, &
      bgw_bcast_real_4, &
      bgw_bcast_real_5, &
      bgw_bcast_real_6, &
      bgw_bcast_real_7, &
      bgw_bcast_complex_0, &
      bgw_bcast_complex_1, &
      bgw_bcast_complex_2, &
      bgw_bcast_complex_3, &
      bgw_bcast_complex_4, &
      bgw_bcast_complex_5, &
      bgw_bcast_complex_6, &
      bgw_bcast_complex_7, &
      bgw_bcast_logical_0, &
      bgw_bcast_logical_1, &
      bgw_bcast_logical_2, &
      bgw_bcast_logical_3, &
      bgw_bcast_logical_4, &
      bgw_bcast_logical_5, &
      bgw_bcast_logical_6, &
      bgw_bcast_logical_7
  end interface

  public :: bgw_bcast

contains

subroutine bgw_bcast_int_0(x, root, comm)
  integer, intent(inout), target :: x
  integer, intent(in), optional :: root, comm

  integer, pointer :: x_1d(:)

  PUSH_SUB(bgw_bcast_int_0)
  call c_f_pointer(c_loc(x), x_1d, [1])
  call bgw_bcast_int_lowlevel(x_1d, 1, root=root, comm=comm)
  POP_SUB(bgw_bcast_int_0)

end subroutine bgw_bcast_int_0

subroutine bgw_bcast_int_1(x, root, comm)
  integer, intent(inout) :: x(:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_int_1)
  call bgw_bcast_int_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_int_1)

end subroutine bgw_bcast_int_1

subroutine bgw_bcast_int_2(x, root, comm)
  integer, intent(inout) :: x(:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_int_2)
  call bgw_bcast_int_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_int_2)

end subroutine bgw_bcast_int_2

subroutine bgw_bcast_int_3(x, root, comm)
  integer, intent(inout) :: x(:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_int_3)
  call bgw_bcast_int_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_int_3)

end subroutine bgw_bcast_int_3

subroutine bgw_bcast_int_4(x, root, comm)
  integer, intent(inout) :: x(:,:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_int_4)
  call bgw_bcast_int_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_int_4)

end subroutine bgw_bcast_int_4

subroutine bgw_bcast_int_5(x, root, comm)
  integer, intent(inout) :: x(:,:,:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_int_5)
  call bgw_bcast_int_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_int_5)

end subroutine bgw_bcast_int_5

subroutine bgw_bcast_int_6(x, root, comm)
  integer, intent(inout) :: x(:,:,:,:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_int_6)
  call bgw_bcast_int_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_int_6)

end subroutine bgw_bcast_int_6

subroutine bgw_bcast_int_7(x, root, comm)
  integer, intent(inout) :: x(:,:,:,:,:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_int_7)
  call bgw_bcast_int_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_int_7)

end subroutine bgw_bcast_int_7

subroutine bgw_bcast_real_0(x, root, comm)
  real(DP), intent(inout), target :: x
  integer, intent(in), optional :: root, comm

  real(DP), pointer :: x_1d(:)

  PUSH_SUB(bgw_bcast_real_0)
  call c_f_pointer(c_loc(x), x_1d, [1])
  call bgw_bcast_real_lowlevel(x_1d, 1, root=root, comm=comm)
  POP_SUB(bgw_bcast_real_0)

end subroutine bgw_bcast_real_0

subroutine bgw_bcast_real_1(x, root, comm)
  real(DP), intent(inout) :: x(:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_real_1)
  call bgw_bcast_real_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_real_1)

end subroutine bgw_bcast_real_1

subroutine bgw_bcast_real_2(x, root, comm)
  real(DP), intent(inout) :: x(:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_real_2)
  call bgw_bcast_real_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_real_2)

end subroutine bgw_bcast_real_2

subroutine bgw_bcast_real_3(x, root, comm)
  real(DP), intent(inout) :: x(:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_real_3)
  call bgw_bcast_real_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_real_3)

end subroutine bgw_bcast_real_3

subroutine bgw_bcast_real_4(x, root, comm)
  real(DP), intent(inout) :: x(:,:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_real_4)
  call bgw_bcast_real_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_real_4)

end subroutine bgw_bcast_real_4

subroutine bgw_bcast_real_5(x, root, comm)
  real(DP), intent(inout) :: x(:,:,:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_real_5)
  call bgw_bcast_real_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_real_5)

end subroutine bgw_bcast_real_5

subroutine bgw_bcast_real_6(x, root, comm)
  real(DP), intent(inout) :: x(:,:,:,:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_real_6)
  call bgw_bcast_real_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_real_6)

end subroutine bgw_bcast_real_6

subroutine bgw_bcast_real_7(x, root, comm)
  real(DP), intent(inout) :: x(:,:,:,:,:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_real_7)
  call bgw_bcast_real_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_real_7)

end subroutine bgw_bcast_real_7

subroutine bgw_bcast_complex_0(x, root, comm)
  complex(DPC), intent(inout), target :: x
  integer, intent(in), optional :: root, comm

  complex(DPC), pointer :: x_1d(:)

  PUSH_SUB(bgw_bcast_complex_0)
  call c_f_pointer(c_loc(x), x_1d, [1])
  call bgw_bcast_complex_lowlevel(x_1d, 1, root=root, comm=comm)
  POP_SUB(bgw_bcast_complex_0)

end subroutine bgw_bcast_complex_0

subroutine bgw_bcast_complex_1(x, root, comm)
  complex(DPC), intent(inout) :: x(:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_complex_1)
  call bgw_bcast_complex_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_complex_1)

end subroutine bgw_bcast_complex_1

subroutine bgw_bcast_complex_2(x, root, comm)
  complex(DPC), intent(inout) :: x(:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_complex_2)
  call bgw_bcast_complex_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_complex_2)

end subroutine bgw_bcast_complex_2

subroutine bgw_bcast_complex_3(x, root, comm)
  complex(DPC), intent(inout) :: x(:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_complex_3)
  call bgw_bcast_complex_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_complex_3)

end subroutine bgw_bcast_complex_3

subroutine bgw_bcast_complex_4(x, root, comm)
  complex(DPC), intent(inout) :: x(:,:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_complex_4)
  call bgw_bcast_complex_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_complex_4)

end subroutine bgw_bcast_complex_4

subroutine bgw_bcast_complex_5(x, root, comm)
  complex(DPC), intent(inout) :: x(:,:,:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_complex_5)
  call bgw_bcast_complex_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_complex_5)

end subroutine bgw_bcast_complex_5

subroutine bgw_bcast_complex_6(x, root, comm)
  complex(DPC), intent(inout) :: x(:,:,:,:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_complex_6)
  call bgw_bcast_complex_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_complex_6)

end subroutine bgw_bcast_complex_6

subroutine bgw_bcast_complex_7(x, root, comm)
  complex(DPC), intent(inout) :: x(:,:,:,:,:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_complex_7)
  call bgw_bcast_complex_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_complex_7)

end subroutine bgw_bcast_complex_7

subroutine bgw_bcast_logical_0(x, root, comm)
  logical, intent(inout), target :: x
  integer, intent(in), optional :: root, comm

  logical, pointer :: x_1d(:)

  PUSH_SUB(bgw_bcast_logical_0)
  call c_f_pointer(c_loc(x), x_1d, [1])
  call bgw_bcast_logical_lowlevel(x_1d, 1, root=root, comm=comm)
  POP_SUB(bgw_bcast_logical_0)

end subroutine bgw_bcast_logical_0

subroutine bgw_bcast_logical_1(x, root, comm)
  logical, intent(inout) :: x(:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_logical_1)
  call bgw_bcast_logical_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_logical_1)

end subroutine bgw_bcast_logical_1

subroutine bgw_bcast_logical_2(x, root, comm)
  logical, intent(inout) :: x(:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_logical_2)
  call bgw_bcast_logical_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_logical_2)

end subroutine bgw_bcast_logical_2

subroutine bgw_bcast_logical_3(x, root, comm)
  logical, intent(inout) :: x(:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_logical_3)
  call bgw_bcast_logical_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_logical_3)

end subroutine bgw_bcast_logical_3

subroutine bgw_bcast_logical_4(x, root, comm)
  logical, intent(inout) :: x(:,:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_logical_4)
  call bgw_bcast_logical_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_logical_4)

end subroutine bgw_bcast_logical_4

subroutine bgw_bcast_logical_5(x, root, comm)
  logical, intent(inout) :: x(:,:,:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_logical_5)
  call bgw_bcast_logical_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_logical_5)

end subroutine bgw_bcast_logical_5

subroutine bgw_bcast_logical_6(x, root, comm)
  logical, intent(inout) :: x(:,:,:,:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_logical_6)
  call bgw_bcast_logical_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_logical_6)

end subroutine bgw_bcast_logical_6

subroutine bgw_bcast_logical_7(x, root, comm)
  logical, intent(inout) :: x(:,:,:,:,:,:,:)
  integer, intent(in), optional :: root, comm

  PUSH_SUB(bgw_bcast_logical_7)
  call bgw_bcast_logical_lowlevel(x, size(x), root=root, comm=comm)
  POP_SUB(bgw_bcast_logical_7)

end subroutine bgw_bcast_logical_7


! Implementation

subroutine bgw_bcast_int_lowlevel(x, sz, root, comm)
  integer, intent(inout) :: x(*)
  integer, intent(in) :: sz
  integer, intent(in), optional :: root, comm

  integer :: root_, comm_, error

  PUSH_SUB(bgw_bcast_int_lowlevel)

#ifdef MPI
  if (peinf%npes>1) then
    root_ = 0
    comm_ = MPI_COMM_WORLD
    if (present(root)) root_ = root
    if (present(comm)) comm_ = comm
    call MPI_Bcast(x, sz, MPI_INTEGER, root_, comm_, error)
    if (error/=0) then
      if ((peinf%inode==root_.and.comm_==MPI_COMM_WORLD) .or. &
        comm_/=MPI_COMM_WORLD) then
        write(0,'(/a)') 'Error in bgw_bcast_int_lowlevel:'
        write(0,'(a,i0/)') 'In call to MPI_Bcast, got error=',error
      endif
      call die('Got error/=0 for MPI_Bcast in bgw_bcast_int_lowlevel', &
        only_root_writes=comm_==MPI_COMM_WORLD)
    endif
  endif
#endif

  POP_SUB(bgw_bcast_int_lowlevel)

end subroutine bgw_bcast_int_lowlevel

subroutine bgw_bcast_real_lowlevel(x, sz, root, comm)
  real(DP), intent(inout) :: x(*)
  integer, intent(in) :: sz
  integer, intent(in), optional :: root, comm

  integer :: root_, comm_, error

  PUSH_SUB(bgw_bcast_real_lowlevel)

#ifdef MPI
  if (peinf%npes>1) then
    root_ = 0
    comm_ = MPI_COMM_WORLD
    if (present(root)) root_ = root
    if (present(comm)) comm_ = comm
    call MPI_Bcast(x, sz, MPI_REAL_DP, root_, comm_, error)
    if (error/=0) then
      if ((peinf%inode==root_.and.comm_==MPI_COMM_WORLD) .or. &
        comm_/=MPI_COMM_WORLD) then
        write(0,'(/a)') 'Error in bgw_bcast_real_lowlevel:'
        write(0,'(a,i0/)') 'In call to MPI_Bcast, got error=',error
      endif
      call die('Got error/=0 for MPI_Bcast in bgw_bcast_real_lowlevel', &
        only_root_writes=comm_==MPI_COMM_WORLD)
    endif
  endif
#endif

  POP_SUB(bgw_bcast_real_lowlevel)

end subroutine bgw_bcast_real_lowlevel

subroutine bgw_bcast_complex_lowlevel(x, sz, root, comm)
  complex(DPC), intent(inout) :: x(*)
  integer, intent(in) :: sz
  integer, intent(in), optional :: root, comm

  integer :: root_, comm_, error

  PUSH_SUB(bgw_bcast_complex_lowlevel)

#ifdef MPI
  if (peinf%npes>1) then
    root_ = 0
    comm_ = MPI_COMM_WORLD
    if (present(root)) root_ = root
    if (present(comm)) comm_ = comm
    call MPI_Bcast(x, sz, MPI_COMPLEX_DPC, root_, comm_, error)
    if (error/=0) then
      if ((peinf%inode==root_.and.comm_==MPI_COMM_WORLD) .or. &
        comm_/=MPI_COMM_WORLD) then
        write(0,'(/a)') 'Error in bgw_bcast_complex_lowlevel:'
        write(0,'(a,i0/)') 'In call to MPI_Bcast, got error=',error
      endif
      call die('Got error/=0 for MPI_Bcast in bgw_bcast_complex_lowlevel', &
        only_root_writes=comm_==MPI_COMM_WORLD)
    endif
  endif
#endif

  POP_SUB(bgw_bcast_complex_lowlevel)

end subroutine bgw_bcast_complex_lowlevel

subroutine bgw_bcast_logical_lowlevel(x, sz, root, comm)
  logical, intent(inout) :: x(*)
  integer, intent(in) :: sz
  integer, intent(in), optional :: root, comm

  integer :: root_, comm_, error

  PUSH_SUB(bgw_bcast_logical_lowlevel)

#ifdef MPI
  if (peinf%npes>1) then
    root_ = 0
    comm_ = MPI_COMM_WORLD
    if (present(root)) root_ = root
    if (present(comm)) comm_ = comm
    call MPI_Bcast(x, sz, MPI_LOGICAL, root_, comm_, error)
    if (error/=0) then
      if ((peinf%inode==root_.and.comm_==MPI_COMM_WORLD) .or. &
        comm_/=MPI_COMM_WORLD) then
        write(0,'(/a)') 'Error in bgw_bcast_logical_lowlevel:'
        write(0,'(a,i0/)') 'In call to MPI_Bcast, got error=',error
      endif
      call die('Got error/=0 for MPI_Bcast in bgw_bcast_logical_lowlevel', &
        only_root_writes=comm_==MPI_COMM_WORLD)
    endif
  endif
#endif

  POP_SUB(bgw_bcast_logical_lowlevel)

end subroutine bgw_bcast_logical_lowlevel


end module bgw_mpi_m

!==============================================================================
!
! Module Avc_m
!
! Created by GKA (2018).
! Gathers routines previously implemented by MLT,JRD,CHP.
!
! Objects holding the exciton wavefunctions, with corresponding io routines.
!
!==============================================================================


#include "f_defs.h"

module evecs_m

#ifdef HDF5
use hdf5
use hdf5_io_m
#endif
use global_m

implicit none

public

! =========================================================================== !

!> BSE eigenvectors and eigenvalues
type evecs_t

  ! Dimensions
  integer :: nmat         !< Size of the BSE hamiltonian
  integer :: usize        !< Size of the eigenvectors (nmat or 2*nmat)
  integer :: nk           !< Number of k-points
  integer :: nc           !< Number of conduction bands
  integer :: nv           !< Number of valence bands
  integer :: ns           !< Number of spins
  integer :: neig         !< Total number of eigenvectors
  integer :: neig_me=1    !< Number of local eigenvectors
  integer :: meig=1       !< Maximum number of local eigenvectors (array dimension)

  logical :: tda = .true. !< Is Tamm-Dancoff approximation used?

  ! Parameters
  integer :: uur = 11     !< Unit number for right eigenvectors
  integer :: uul = 12     !< Unit number for left eigenvectors

  !> k-points
  !! kk(3,nk)
  real(DP), allocatable :: kk(:,:)

  !> Eigenvalues.
  !! Note that this array is not distributed; each node has all eigenvalues
  !! evals(neig)
  real(DP), allocatable :: evals(:)

  ! The eigenvectors are named u when they are stored as flat arrays
  ! and Avc when they are stored as (s,v,c,ik)

  !> Right eigenvectors (flat)
  !! u_r(nmat,meig) or ur(2*nmat,meig)
  SCALAR, allocatable :: u_r(:,:)

  !> Left eigenvectors (flat)
  !! u_l(2*nmat,meig)
  SCALAR, allocatable :: u_l(:,:)

  !> Right e-h coefficients
  !! Avc_r(ns,nv,nc,nk,meig)
  SCALAR, allocatable :: Avc_r(:,:,:,:,:)

  !> Left e-h coefficients
  !! Avc_l(ns,nv,nc,nk,meig)
  SCALAR, allocatable :: Avc_l(:,:,:,:,:)

  !> Right e-h coefficients, emission component
  !! Bvc_r(ns,nv,nc,nk,meig)
  SCALAR, allocatable :: Bvc_r(:,:,:,:,:)

  !> Left e-h coefficients, emission component
  !! Bvc_l(ns,nv,nc,nk,meig)
  SCALAR, allocatable :: Bvc_l(:,:,:,:,:)

  contains

  procedure, pass :: init => init_evecs
  procedure, pass :: alloc => alloc_evecs
  procedure, pass :: free => free_evecs
  procedure, pass :: reshape_Avc

  ! Reading routines
  procedure, pass :: open_read_eigenvectors_bin
  procedure, pass :: read_header_bin
  procedure, pass :: read_single_eigenvectors_bin
  procedure, pass :: close_eigenvectors_bin

  ! Writing routines
  procedure, pass :: write_eigenvectors_bin

end type evecs_t

! =========================================================================== !
contains
! =========================================================================== !

!> Initialize the eigenvectors
subroutine init_evecs(this, xct, kg, nmat, neig, meig)
  class(evecs_t), intent(inout) :: this
  type (xctinfo), intent(in) :: xct   !< Information on the BSE calculation
  type (grid), intent(in) :: kg       !< Information on the k-point grid
  integer, intent(in) :: nmat
  integer, intent(in) :: neig
  integer, intent(in) :: meig

  integer :: ii, ik

  PUSH_SUB(init_evecs)

  this%nmat = nmat
  this%neig = neig
  this%meig = meig

  this%ns = xct%nspin
  this%nv = xct%nvb_fi
  this%nc = xct%ncb_fi
  this%nk = xct%nkpt_fi
  this%tda = xct%tda

  SAFE_ALLOCATE(this%kk, (3, this%nk))
  this%kk = kg%f

  POP_SUB(init_evecs)

end subroutine init_evecs

! =========================================================================== !

!> Allocate memory
subroutine alloc_evecs(this, with_Avc)
  class(evecs_t), intent(inout) :: this
  logical, intent(in),optional :: with_Avc  !< Also allocate e-h coefficients

  logical :: allocate_Avc = .false.

  PUSH_SUB(alloc_evecs)

  if (present(with_Avc)) allocate_Avc = with_Avc

  SAFE_ALLOCATE(this%evals, (this%neig))

  if ( this%tda ) then
    this%usize = this%nmat
    SAFE_ALLOCATE(this%u_r, (this%usize,this%meig))
    if (allocate_Avc) then
      SAFE_ALLOCATE(this%Avc_r, (this%ns,this%nv,this%nc,this%nk,this%meig))
    end if
  else

    this%usize = 2*this%nmat
    SAFE_ALLOCATE(this%u_r, (this%usize, this%meig))
    SAFE_ALLOCATE(this%u_l, (this%usize, this%meig))
    if (allocate_Avc) then
      SAFE_ALLOCATE(this%Avc_r, (this%ns,this%nv,this%nc,this%nk, this%meig))
      SAFE_ALLOCATE(this%Avc_l, (this%ns,this%nv,this%nc,this%nk, this%meig))
      SAFE_ALLOCATE(this%Bvc_r, (this%ns,this%nv,this%nc,this%nk, this%meig))
      SAFE_ALLOCATE(this%Bvc_l, (this%ns,this%nv,this%nc,this%nk, this%meig))
    end if

  end if

  if (.not. allocated(this%kk)) then
    SAFE_ALLOCATE(this%kk, (3, this%nk))
  end if

  POP_SUB(alloc_evecs)

end subroutine alloc_evecs

! =========================================================================== !

!> Free memory
subroutine free_evecs(this)
  class(evecs_t), intent(inout) :: this

  PUSH_SUB(free_evecs)

  SAFE_DEALLOCATE(this%kk)
  SAFE_DEALLOCATE(this%evals)
  SAFE_DEALLOCATE(this%u_r)
  SAFE_DEALLOCATE(this%u_l)
  SAFE_DEALLOCATE(this%Avc_r)
  SAFE_DEALLOCATE(this%Avc_l)
  SAFE_DEALLOCATE(this%Bvc_r)
  SAFE_DEALLOCATE(this%Bvc_l)

  POP_SUB(free_evecs)

end subroutine free_evecs

! =========================================================================== !

!> Transform the flat eigenvectors into the Avc components
subroutine reshape_Avc(this)
  class(evecs_t), intent(inout) :: this

  integer :: ieig,iflat,ik,ic,iv,is

  PUSH_SUB(reshape_Avc)

  if ( this%tda ) then
    do ieig=1,this%neig_me
      iflat = 0
      do ik=1,this%nk
        do ic=1,this%nc
          do iv=1,this%nv
            do is=1,this%ns
              iflat = iflat + 1
              this%Avc_r(is,iv,ic,ik,ieig) = this%u_r(iflat,ieig)
            enddo
          enddo
        enddo
      enddo
    enddo
  else
    do ieig=1,this%neig_me
      iflat = 0
      do ik=1,this%nk
        do ic=1,this%nc
          do iv=1,this%nv
            do is=1,this%ns
              iflat = iflat + 1
              this%Avc_r(is,iv,ic,ik,ieig) = this%u_r(iflat,ieig)
              this%Bvc_r(is,iv,ic,ik,ieig) = this%u_r(this%nmat+iflat,ieig)
              this%Avc_l(is,iv,ic,ik,ieig) = this%u_l(iflat,ieig)
              this%Bvc_l(is,iv,ic,ik,ieig) = this%u_l(this%nmat+iflat,ieig)
            enddo
          enddo
        enddo
      enddo
    enddo
  end if !tda

  POP_SUB(reshape_Avc)

end subroutine reshape_Avc

! =========================================================================== !
! Reading routines
! =========================================================================== !

!> Open the binary eigenvectors file(s) for reading
subroutine open_read_eigenvectors_bin(this, rootname)
  class(evecs_t), intent(inout) :: this
  character(*), intent(in), optional :: rootname

  character(60) :: fname

  PUSH_SUB(open_read_eigenvectors_bin)

  if (present(rootname)) then
    fname = trim(rootname)
  else
    fname = 'eigenvectors'
  end if

  if (this%tda) then 
    call open_file(unit=this%uur,file=trim(fname),form='unformatted',status='old')
  else
    call open_file(unit=this%uul,file=trim(trim(fname)//'_l'),form='unformatted',status='old')
    call open_file(unit=this%uur,file=trim(trim(fname)//'_r'),form='unformatted',status='old')
  end if

  POP_SUB(open_read_eigenvectors_bin)

end subroutine open_read_eigenvectors_bin

! =========================================================================== !

!> Read the dimensions from binary file(s)
subroutine read_header_bin(this)
  class(evecs_t), intent(inout) :: this

  real(DP), allocatable :: kk_l(:,:)
  integer :: ns_, nv_, nc_, nk_, nmat_
  integer :: ik

  PUSH_SUB(read_header_bin)

  read(this%uur) this%ns
  read(this%uur) this%nv
  read(this%uur) this%nc
  read(this%uur) this%nk
  this%nmat = this%ns*this%nv*this%nc*this%nk
  this%neig = this%nmat
  this%meig = this%nmat
  if (.not. this%tda ) then
    read(this%uul) ns_
    read(this%uul) nv_
    read(this%uul) nc_
    read(this%uul) nk_
    nmat_ = ns_*nv_*nc_*nk_
    if( this%nmat .ne. nmat_) then
      call die('Error found: nmat should be equal for left and right eigenvalue matrices')
    end if
  end if

  SAFE_ALLOCATE(this%kk, (3,this%nk))
  read(this%uur) this%kk(:,:)
  if( .not. this%tda ) then
    SAFE_ALLOCATE(kk_l, (3,this%nk))
    read(this%uul) kk_l(:,:)
!   Check:
    do ik=1,this%nk
      ! GKA: FIXME This check doesnt make sense
      if( abs(this%kk(1,ik)-kk_l(1,ik)) > 1.d06) then
        if( abs(this%kk(2,ik)-kk_l(2,ik)) > 1.d06) then
          if( abs(this%kk(3,ik)-kk_l(3,ik)) > 1.d06) then
            call die('Inconsistency in k-points found in left and right eigenvalues')
          end if
        end if
      end if 
    end do
    SAFE_DEALLOCATE(kk_l)
  end if
  
  POP_SUB(read_header_bin)

end subroutine read_header_bin

! =========================================================================== !

!> Read a single eigenvalue and eigenvector(s)
subroutine read_single_eigenvectors_bin(this, ieig)
  class(evecs_t), intent(inout) :: this
  integer, intent(in), optional :: ieig  !< Index of the eigenvector for storage

  integer :: ii
  real(DP) :: eval_l

  PUSH_SUB(read_single_eigenvectors_bin)

  ii = 1
  if (present(ieig)) ii = ieig

  if ( this%tda ) then
     read(this%uur) this%evals(ii)
     read(this%uur) this%u_r(:,ii)
  else
     read(this%uur) this%evals(ii)
     read(this%uur) this%u_r(:,ii)
     read(this%uul) eval_l
     read(this%uul) this%u_l(:,ii)

     if( abs(eval_l - this%evals(ii))>1.d-12) then
        call die('Inconsistency in energies in left and right eigenfuncion files')
     end if
  end if

  POP_SUB(read_single_eigenvectors_bin)

end subroutine read_single_eigenvectors_bin

! =========================================================================== !

!> Close the binary eigenvectors file(s)
subroutine close_eigenvectors_bin(this)
  class(evecs_t), intent(inout) :: this

  PUSH_SUB(close_eigenvectors_bin)

  call close_file(this%uur)
  if ( .not. this%tda) call close_file(this%uul)

  POP_SUB(close_eigenvectors_bin)

end subroutine close_eigenvectors_bin

! =========================================================================== !
! Writing routines
! =========================================================================== !

!> Write the binary eigenvectors file(s)
subroutine write_eigenvectors_bin(this, nwrite)
  class(evecs_t), intent(inout) :: this
  integer, intent(inout) :: nwrite

  integer :: ieig,ii,jj,ik,peadd
  integer :: rank_r, rank_l
  logical :: io_r, io_l
  character(len=128) :: fname_r, fname_l
  SCALAR, allocatable :: u_r(:), u_l(:)

  PUSH_SUB(write_eigenvectors_bin)

! Who can do io?
! FHJ: we do i/o for the right and left eigenvectors using different MPI ranks.
! The hope is to get better load balance on a lustre FS. The optimal solution,
! however, is to use HDF5.
  rank_r = 0
  io_r = peinf%inode==rank_r
  rank_l = peinf%npes-1
  io_l = (peinf%inode==rank_l) .and. (.not. this%tda)

  if (nwrite.lt.0) nwrite=this%neig

  ! Temporary arrays for communication
  SAFE_ALLOCATE(u_r, (this%usize))
  if (.not. this%tda) then
    SAFE_ALLOCATE(u_l, (this%usize))
  end if

! Open the file we will write to and write header information

  if (io_r) then
    write(6,*)
    if (.not. this%tda) then
      fname_r = "eigenvectors_r"
      write(6,'(1x,a,i0,a)') 'Writing ',nwrite, ' right eigenvectors to file "'//trim(fname_r)//'"'
    else
      fname_r = "eigenvectors"
      write(6,'(1x,a,i0,a)') 'Writing ',nwrite, ' eigenvectors to file "'//trim(fname_r)//'"'
    endif
    write(6,'(1x,a,i0)') 'Length of each vector: ', this%usize
    write(6,*)
    call open_file(unit=this%uur,file=trim(fname_r),form='unformatted',status='replace')
    write(this%uur) this%ns
    write(this%uur) this%nv
    write(this%uur) this%nc
    write(this%uur) this%nk
    write(this%uur) ((this%kk(jj,ik),jj=1,3),ik=1,this%nk)
  endif
  if (io_l) then
    fname_l = "eigenvectors_l"
    write(6,*)
    write(6,'(1x,a,i0,a)') 'Writing ',nwrite, ' left eigenvectors to file "eigenvectors_l"'
    write(6,'(1x,a,i0)') 'Length of each vector: ', this%usize
    write(6,*)
    call open_file(unit=this%uul,file=trim(fname_l),form='unformatted',status='replace')
    write(this%uul) this%ns
    write(this%uul) this%nv
    write(this%uul) this%nc
    write(this%uul) this%nk
    write(this%uul) ((this%kk(jj,ik),jj=1,3),ik=1,this%nk)
  endif

! Loop over states to be written

  do ieig=1,nwrite

! Figure out which processor (peadd) and column
! state ieig belongs to

    peadd_loop: do peadd=1,peinf%npes
      do jj=1,peinf%neig(peadd)
        if (peinf%peig(peadd,jj).eq.ieig) then

! Get the coeffs for state ieig into A (on all processors)
          if (peinf%inode==peadd-1) then
            u_r(:) = this%u_r(:,jj)
#ifdef MPI
            if (peadd-1/=rank_r) then
              call MPI_Send(u_r(1), this%usize, MPI_SCALAR, rank_r, ieig, &
                MPI_COMM_WORLD, mpierr)
            endif
#endif
            if (.not. this%tda) then
              u_l(:) = this%u_l(:,jj)
#ifdef MPI
              if (peadd-1/=rank_l) then
                call MPI_Send(u_l(1), this%usize, MPI_SCALAR, rank_l, ieig+nwrite, &
                  MPI_COMM_WORLD, mpierr)
              endif
#endif
            endif
          endif
#ifdef MPI
          if (io_r.and.peadd-1/=rank_r) then
            call MPI_Recv(u_r(1), this%usize, MPI_SCALAR, peadd-1, ieig, &
              MPI_COMM_WORLD, MPI_STATUS_IGNORE, mpierr)
          endif
          if (io_l.and.peadd-1/=rank_l) then
            call MPI_Recv(u_l(1), this%usize, MPI_SCALAR, peadd-1, ieig+nwrite, &
              MPI_COMM_WORLD, MPI_STATUS_IGNORE, mpierr)
          endif
#endif

! Write to file

          if (io_r) then
            if (peinf%verb_debug) then
              if (.not. this%tda) then
                write(6,'(1x,a,i0,a,f0.6)') 'Writing right state ',ieig,' energy = ',this%evals(ieig)
              else
                write(6,'(1x,a,i0,a,f0.6)') 'Writing state ',ieig,' energy = ',this%evals(ieig)
              endif
            endif
            write(this%uur) this%evals(ieig)
            write(this%uur) (u_r(ii),ii=1,this%usize)
          endif
          if (io_l) then
            if (peinf%verb_debug) then
              write(6,'(1x,a,i0,a,f0.6)') 'Writing left state ',ieig,' energy = ',this%evals(ieig)
            endif
            write(this%uul) this%evals(ieig)
            write(this%uul) (u_l(ii),ii=1,this%usize)
          endif
        endif
      enddo
    enddo peadd_loop

  enddo ! of loop over states (ieig)
  
  if (io_r) call close_file(this%uur)
  if (io_l) call close_file(this%uul)

  SAFE_DEALLOCATE(u_r)
  SAFE_DEALLOCATE(u_l)
  
  POP_SUB(write_eigenvectors_bin)
  
  return
  
end subroutine write_eigenvectors_bin


end module evecs_m

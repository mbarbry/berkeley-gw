! ${mako_signature}
! ${dont_edit}

!>=========================================================================
!!
!! Module:
!!
!! bgw_mpi_m        Originally by FHJ     Last Modified 03/2018 (FHJ)
!!
!!     Wrappers around MPI functions. All functions work both with
!!     MPI and serial runs. Right now, only MPI_Bcast is supported.
!!     TODO: implement wrappers for MPI_Send, MPI_Recv, gather, etc.
!!
!!     This module uses python Mako template to generate Fortran code.
!!     Make sure to run the `mako_preprocess.py` if you edit the source
!!     .f90p file, and don`t edit the generated .f90 directly.
!!
!!=========================================================================

#include "f_defs.h"

<%
import itertools
#subs = ('bcast', 'send', 'recv')
subs = ('bcast',)
dtypes = ('int', 'real', 'complex', 'logical')
ranks = range(8)
dtype2fortran = {
  'int': 'integer',
  'real': 'real(DP)',
  'complex': 'complex(DPC)',
  'logical': 'logical'
}

dtype2MPI = {
  'int': 'MPI_INTEGER',
  'real': 'MPI_REAL_DP',
  'complex': 'MPI_COMPLEX_DPC',
  'logical': 'MPI_LOGICAL'
}

def rank2array(rank):
  assert rank>=0
  if rank==0:
    return ''
  else:
    return '(' + ','.join([':']*rank) + ')'

def get_label(sub, dtype, rank):
  assert sub in subs
  assert dtype in dtypes
  assert rank in ranks or rank=='lowlevel'
  return 'bgw_{}_{}_{}'.format(sub, dtype, rank)
%>\

module bgw_mpi_m
  use, intrinsic :: iso_c_binding
  use global_m

  implicit none

  interface bgw_bcast
    module procedure &
%for sub, dtype, rank in itertools.product(subs, dtypes, ranks):
%if loop.index>0:
, &
%endif
      ${get_label(sub, dtype, rank)}\
%endfor

  end interface

  public :: bgw_bcast

contains

%for sub, dtype, rank in itertools.product(subs, dtypes, ranks):
<%
  label_lowlevel = get_label(sub, dtype, 'lowlevel')
  label = get_label(sub, dtype, rank)
  dtype_fortran = dtype2fortran[dtype]
  if rank==0:
    target = ', target'
  else:
    target = ''
%>\
subroutine ${label}(x, root, comm)
  ${dtype_fortran}, intent(inout)${target} :: x${rank2array(rank)}
  integer, intent(in), optional :: root, comm
%if rank==0:

  ${dtype2fortran[dtype]}, pointer :: x_1d(:)
%endif

  PUSH_SUB(${label})
%if rank==0:
  call c_f_pointer(c_loc(x), x_1d, [1])
  call ${label_lowlevel}(x_1d, 1, root=root, comm=comm)
%else:
  call ${label_lowlevel}(x, size(x), root=root, comm=comm)
%endif
  POP_SUB(${label})

end subroutine ${label}

%endfor

! Implementation

%for dtype in dtypes:
<%
  sub = 'bcast'
  label = get_label(sub, dtype, 'lowlevel')
  dtype_mpi = dtype2MPI[dtype]
  dtype_fortran = dtype2fortran[dtype]
%>\
subroutine ${label}(x, sz, root, comm)
  ${dtype_fortran}, intent(inout) :: x(*)
  integer, intent(in) :: sz
  integer, intent(in), optional :: root, comm

  integer :: root_, comm_, error

  PUSH_SUB(${label})

#ifdef MPI
  if (peinf%npes>1) then
    root_ = 0
    comm_ = MPI_COMM_WORLD
    if (present(root)) root_ = root
    if (present(comm)) comm_ = comm
%if sub=='bcast':
    call MPI_Bcast(x, sz, ${dtype_mpi}, root_, comm_, error)
%endif
    if (error/=0) then
      if ((peinf%inode==root_.and.comm_==MPI_COMM_WORLD) .or. &
        comm_/=MPI_COMM_WORLD) then
        write(0,'(/a)') 'Error in ${label}:'
        write(0,'(a,i0/)') 'In call to MPI_Bcast, got error=',error
      endif
      call die('Got error/=0 for MPI_Bcast in ${label}', &
        only_root_writes=comm_==MPI_COMM_WORLD)
    endif
  endif
#endif

  POP_SUB(${label})

end subroutine ${label}

%endfor

end module bgw_mpi_m

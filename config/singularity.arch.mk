# arch.mk for BerkeleyGW codes
#
# Suitable for Generic single processor linux 
# Tested on laptop running Ubuntu 12.04.  You will need to have yum 
# or apt-get installed liblapack-dev, fftw-dev, gfortran, g++, 
# libhdf5-serial-dev packages
#
# J. Deslippe
# Jun, 2013, NERSC

COMPFLAG  = -DGNU
PARAFLAG  =
MATHFLAG  = -DUSEFFTW3 -DUNPACKED
# Only uncomment DEBUGFLAG if you need to develop/debug BerkeleyGW.
# The output will be much more verbose, and the code will slow down by ~20%.
#DEBUGFLAG = -DDEBUG

MKLROOT      = /opt/conda/lib/mkl
HDF5_ROOT    = /opt/hdf5
BGWROOT 		 = /opt/berkeley-gw

#########################################################################
#   NOTE: This arch.mk is used by a buildslave. The compiler flags are  #
#   optimized to debug the code and not for code performance.           #
#########################################################################
FCPP    = cpp -C -nostdinc
F90free = gfortran -ffree-form -ffree-line-length-none -std=gnu -fopenmp -Wall
#F90free = gfortran -ffree-form -ffree-line-length-none -fbounds-check -Wall -pedantic-errors -std=gnu
LINK    = gfortran -fopenmp
FOPTS   = -O3
FNOOPTS = $(FOPTS)
MOD_OPT = -J
INCFLAG = -I${HDF5_ROOT}/include -I${BGWROOT}/Common

CC_COMP = g++ -Wall -pedantic-errors -std=c++0x
C_COMP  = gcc -Wall -pedantic-errors -std=c99
C_LINK  = g++
C_OPTS  = -O3
C_DEBUGFLAG =

REMOVE  = /bin/rm -f

# Math Libraries
#
FFTWLIB      = -lfftw3
FFTWINCLUDE  = -I/usr/include
LAPACKLIB    = -Wl,--no-as-needed -L${MKLROOT}/lib -lmkl_gf_lp64 -lmkl_core -lmkl_gnu_thread -lpthread -lm -ldl
HDF5LIB      = -L${HDF5_ROOT}/lib -lhdf5hl_fortran -lhdf5_hl -lhdf5_fortran -lhdf5 -lz
HDF5INCLUDE  = -I${HDF5_ROOT}/include
